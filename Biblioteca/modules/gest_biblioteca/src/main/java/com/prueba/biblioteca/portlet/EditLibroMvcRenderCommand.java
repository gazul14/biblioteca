package com.prueba.biblioteca.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.prueba.biblioteca.constants.BibliotecaPortletKeys;
import com.prueba.biblioteca.model.Libro;
import com.prueba.biblioteca.service.LibroLocalServiceUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BibliotecaPortletKeys.BIBLIOTECA,
                "mvc.command.name=displayLibroEdition"
        },
        service = MVCRenderCommand.class
)
public class EditLibroMvcRenderCommand implements MVCRenderCommand{

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		Long libroId = ParamUtil.getLong(renderRequest, "libroId", 0);

	        try {
	            Libro libro = LibroLocalServiceUtil.getLibro(libroId);
	            renderRequest.setAttribute("libro", libro);

	        } catch (PortalException e) {
	            throw new RuntimeException(e);
	        }

	        return "/editar_libro.jsp";
	}

}
