package com.prueba.biblioteca.portlet;

import com.prueba.biblioteca.constants.BibliotecaPortletKeys;
import com.prueba.biblioteca.model.Libro;
import com.prueba.biblioteca.service.LibroLocalServiceUtil;

import java.io.IOException;
import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author Gabriel E. Gonzalez S.
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Biblioteca",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + BibliotecaPortletKeys.BIBLIOTECA,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class BibliotecaPortlet extends MVCPortlet {
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		String jspPage = ParamUtil.getString(renderRequest, "jspPage", "");
		
		if(jspPage.equals("/lista_libro.jsp")){			
			List<Libro> listaLibro = LibroLocalServiceUtil.getLibros(0, Integer.MAX_VALUE);		
			renderRequest.setAttribute("listaLibros", listaLibro);
		}
		super.render(renderRequest, renderResponse);
	}
	
	public void crearLibro(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		 String titulo = ParamUtil.getString(actionRequest, "titulo", "");
		 String autor = ParamUtil.getString(actionRequest, "autor", "");
		 
		 Libro libro = LibroLocalServiceUtil.createLibro(CounterLocalServiceUtil.increment(Libro.class.getName()));		 
		 libro.setTitulo(titulo);
		 libro.setAutor(autor);		 
		 LibroLocalServiceUtil.updateLibro(libro);	
	}
	
	public void editarLibro(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
			Long libroId = ParamUtil.getLong(actionRequest, "libroId", 0);
			String titulo = ParamUtil.getString(actionRequest, "titulo", "");
			String autor = ParamUtil.getString(actionRequest, "autor", "");

	        try {
				LibroLocalServiceUtil.updateLibro(libroId, titulo, autor);
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	        
	        
	        actionResponse.setRenderParameter("mvcPath", "/view.jsp");
	}
	
	public void eliminarLibro(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		Long id = ParamUtil.getLong(actionRequest, "libroId", 0);
		try {
			LibroLocalServiceUtil.deleteLibro(id);
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
}