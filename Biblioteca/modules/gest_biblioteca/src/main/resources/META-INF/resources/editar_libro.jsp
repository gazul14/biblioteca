<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="./init.jsp" %>

<jsp:useBean id="libro" type="com.prueba.biblioteca.model.Libro" scope="request"/>

<portlet:actionURL name="editarLibro" var="editLibroUrl"/>

<h1>Editar Libro</h1>

<aui:form action="${editLibroUrl}">
	<aui:input name="id" label="Id:" type="text" value="<%=String.valueOf(libro.getLibroId()) %>"/>
    <aui:input name="titulo" label="Titulo:" value="<%=libro.getTitulo() %>"/>
    <aui:input name="autor" label="Autor:" value="<%=libro.getAutor() %>"/>
    <aui:input name="libroId" type="hidden" value="<%=String.valueOf(libro.getLibroId()) %>"/>
    <aui:button name="editLibroButton" type="submit" value="Editar"/>
    <a class="btn  btn-cancel btn-default btn-link" href="<portlet:renderURL />">Cancelar</a>
</aui:form>
