<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/init.jsp" %>

<portlet:renderURL var ='libro'>
	<portlet:param name='jspPage' value='/crear_libro.jsp' />
</portlet:renderURL>

<portlet:renderURL var ='listaLibro'>
	<portlet:param name='jspPage' value='/lista_libro.jsp' />
</portlet:renderURL>


<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link" href="#">Home</a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Biblioteca</a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="${libro}">Crear Libro</a>
      <a class="dropdown-item" href="${listaLibro}">Libros</a>           
  </li>
</ul>









<!--<aui:form action="${libro}">
    <aui:button name="crearLibroButton" type="submit" value="Crear Libro"/>
</aui:form>



<liferay-ui:search-container emptyResultsMessage="No has creado todavía ningún Libro.">
    <liferay-ui:search-container-results results="${listaLibros}"/>

    <liferay-ui:search-container-row className="com.prueba.libro.model.Libro" modelVar="libros">
        <liferay-ui:search-container-column-text name="Titulo" property="titulo"/>
        <liferay-ui:search-container-column-jsp name="Acciones" path="/libroActionButtons.jsp"/>
    </liferay-ui:search-container-row>

    <liferay-ui:search-iterator/>
</liferay-ui:search-container>

<portlet:resourceURL id="downloadLibros" var="downloadLibrossUrl"/>


<aui:form action="${listaLibro}">
    <aui:button name="listLibroButton" type="submit" value="Libros"/>
</aui:form>
-->