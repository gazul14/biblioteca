<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="./init.jsp" %>

<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="com.prueba.biblioteca.model.Libro" %>

<%
    final ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    final Libro libro = (Libro) row.getObject();
%>

<portlet:renderURL var="displayLibroEditionUrl">
    <portlet:param name="mvcRenderCommandName" value="displayLibroEdition" />
    <portlet:param name="libroId" value="<%= String.valueOf(libro.getLibroId()) %>" />
</portlet:renderURL>

<portlet:actionURL name="eliminarLibro" var="eliminarLibroUrl">
    <portlet:param name="libroId" value="<%=String.valueOf(libro.getLibroId())%>" />
</portlet:actionURL>

<liferay-ui:icon-menu>
    <liferay-ui:icon image="edit" message="Editar" url="${displayLibroEditionUrl}" />
    <liferay-ui:icon image="delete" message="Eliminar" url="${eliminarLibroUrl}" />
</liferay-ui:icon-menu>
