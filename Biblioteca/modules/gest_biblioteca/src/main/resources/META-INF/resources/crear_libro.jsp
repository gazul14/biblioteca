<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/init.jsp" %>

<h1>Registro de Libro</h1>



<portlet:actionURL name="crearLibro" var="crearLibroURL"/>
<portlet:actionURL name="cancelar" var="cancelarURL"/>

<aui:form action="${crearLibroURL}">
    <aui:input name="titulo" type="text" label="Ttulo:"/>
    <aui:input name="autor" type="text" label="Autor:"/>
    <aui:button name="crearLibroButton" type="submit" value="Guardar"/>    
    <a class="btn  btn-cancel btn-default btn-link" href="<portlet:renderURL />">Cancelar</a>
</div>
    
</aui:form>


