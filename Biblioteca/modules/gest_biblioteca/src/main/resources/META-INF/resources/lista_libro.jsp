<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/init.jsp" %>

<h1>Libros</h1>

<jsp:useBean id="listaLibros" type="java.util.List<com.prueba.biblioteca.model.Libro>" scope="request"/>

<liferay-ui:search-container emptyResultsMessage="No has creado todavía ningún libro.">
    <liferay-ui:search-container-results results="${listaLibros}"/>
    <liferay-ui:search-container-row className="com.prueba.biblioteca.model.Libro" modelVar="libro">
        <liferay-ui:search-container-column-text name="Id" property="libroId"/>
        <liferay-ui:search-container-column-text name="Titulo" property="titulo"/>
        <liferay-ui:search-container-column-text name="Autor" property="autor"/>
        <liferay-ui:search-container-column-jsp name="Acciones" path="/libroActionButtons.jsp"/>
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator/>
</liferay-ui:search-container>

<a class="btn  btn-cancel btn-default btn-link" href="<portlet:renderURL />">Volver</a>



<!-- <div style="margin-top: 15px;">
	<portlet:resourceURL var="exportCSV" />
	<a href="${exportCSV}">Exportar a CSV</a>
</div> -->


