/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.prueba.biblioteca.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services, specifically {@link com.prueba.biblioteca.service.http.LibroServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LibroSoap implements Serializable {

	public static LibroSoap toSoapModel(Libro model) {
		LibroSoap soapModel = new LibroSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setLibroId(model.getLibroId());
		soapModel.setTitulo(model.getTitulo());
		soapModel.setAutor(model.getAutor());

		return soapModel;
	}

	public static LibroSoap[] toSoapModels(Libro[] models) {
		LibroSoap[] soapModels = new LibroSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LibroSoap[][] toSoapModels(Libro[][] models) {
		LibroSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LibroSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LibroSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LibroSoap[] toSoapModels(List<Libro> models) {
		List<LibroSoap> soapModels = new ArrayList<LibroSoap>(models.size());

		for (Libro model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LibroSoap[soapModels.size()]);
	}

	public LibroSoap() {
	}

	public long getPrimaryKey() {
		return _libroId;
	}

	public void setPrimaryKey(long pk) {
		setLibroId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getLibroId() {
		return _libroId;
	}

	public void setLibroId(long libroId) {
		_libroId = libroId;
	}

	public String getTitulo() {
		return _titulo;
	}

	public void setTitulo(String titulo) {
		_titulo = titulo;
	}

	public String getAutor() {
		return _autor;
	}

	public void setAutor(String autor) {
		_autor = autor;
	}

	private String _uuid;
	private long _libroId;
	private String _titulo;
	private String _autor;

}