/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.prueba.biblioteca.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link Libro}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Libro
 * @generated
 */
@ProviderType
public class LibroWrapper
	extends BaseModelWrapper<Libro> implements Libro, ModelWrapper<Libro> {

	public LibroWrapper(Libro libro) {
		super(libro);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("libroId", getLibroId());
		attributes.put("titulo", getTitulo());
		attributes.put("autor", getAutor());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long libroId = (Long)attributes.get("libroId");

		if (libroId != null) {
			setLibroId(libroId);
		}

		String titulo = (String)attributes.get("titulo");

		if (titulo != null) {
			setTitulo(titulo);
		}

		String autor = (String)attributes.get("autor");

		if (autor != null) {
			setAutor(autor);
		}
	}

	/**
	 * Returns the autor of this libro.
	 *
	 * @return the autor of this libro
	 */
	@Override
	public String getAutor() {
		return model.getAutor();
	}

	/**
	 * Returns the libro ID of this libro.
	 *
	 * @return the libro ID of this libro
	 */
	@Override
	public long getLibroId() {
		return model.getLibroId();
	}

	/**
	 * Returns the primary key of this libro.
	 *
	 * @return the primary key of this libro
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the titulo of this libro.
	 *
	 * @return the titulo of this libro
	 */
	@Override
	public String getTitulo() {
		return model.getTitulo();
	}

	/**
	 * Returns the uuid of this libro.
	 *
	 * @return the uuid of this libro
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the autor of this libro.
	 *
	 * @param autor the autor of this libro
	 */
	@Override
	public void setAutor(String autor) {
		model.setAutor(autor);
	}

	/**
	 * Sets the libro ID of this libro.
	 *
	 * @param libroId the libro ID of this libro
	 */
	@Override
	public void setLibroId(long libroId) {
		model.setLibroId(libroId);
	}

	/**
	 * Sets the primary key of this libro.
	 *
	 * @param primaryKey the primary key of this libro
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the titulo of this libro.
	 *
	 * @param titulo the titulo of this libro
	 */
	@Override
	public void setTitulo(String titulo) {
		model.setTitulo(titulo);
	}

	/**
	 * Sets the uuid of this libro.
	 *
	 * @param uuid the uuid of this libro
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected LibroWrapper wrap(Libro libro) {
		return new LibroWrapper(libro);
	}

}